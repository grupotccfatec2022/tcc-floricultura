package br.com.floricultura.utility;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class MensagensAlerta {
	
	Alert alert = null;
	
	public void mensagemInformativa(String titulo, String cabecalho, String mensagem) {
		alert = new Alert(AlertType.WARNING);
		alert.setTitle(titulo);
		alert.setHeaderText(cabecalho);
		alert.setContentText(mensagem);
		alert.showAndWait();
	}
	
	public boolean mensagemConfirmacao(String titulo, String cabecalho, String mensagem) {
		alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(titulo);
		alert.setHeaderText(cabecalho);
		alert.setContentText(mensagem);
		alert.showAndWait();
		Optional<ButtonType> result = alert.showAndWait();
		
		return result.get() == ButtonType.OK;
	}

	public void mensagemErro(String titulo, String cabecalho, String mensagem) {
		alert = new Alert(AlertType.ERROR);
		alert.setTitle(titulo);
		alert.setHeaderText(cabecalho);
		alert.setContentText(mensagem);
		alert.showAndWait();
	}
}
