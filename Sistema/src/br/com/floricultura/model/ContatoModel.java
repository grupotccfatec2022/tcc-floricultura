package br.com.floricultura.model;

public class ContatoModel {
	int codContato;
	String celular;
	String telefone;
	String email;
	
	public int getCodContato() {
		return codContato;
	}
	public void setCodContato(int codContato) {
		this.codContato = codContato;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
