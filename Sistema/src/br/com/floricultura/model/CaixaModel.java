package br.com.floricultura.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public class CaixaModel {
	
	int codCaixa;
	BigDecimal valorInicial;
	BigDecimal valorFinal;
	LocalDate dataEntradaCaixa;
	String usuario;
	String statusCaixa;
	

	public int getCodCaixa() {
		return codCaixa;
	}
	public void setCodCaixa(int codCaixa) {
		this.codCaixa = codCaixa;
	}
	public BigDecimal getValorInicial() {
		return valorInicial;
	}
	public void setValorInicial(BigDecimal valorIncial) {
		this.valorInicial = valorIncial;
	}
	public BigDecimal getValorFinal() {
		return valorFinal;
	}
	public void setValorFinal(BigDecimal valorFinal) {
		this.valorFinal = valorFinal;
	}
	public LocalDate getDataEntradaCaixa() {
		return dataEntradaCaixa;
	}
	public void setDataEntradaCaixa(LocalDate dataEntradaCaixa) {
		this.dataEntradaCaixa = dataEntradaCaixa;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getStatusCaixa() {
		return statusCaixa;
	}
	public void setStatusCaixa(String statusCaixa) {
		this.statusCaixa = statusCaixa;
	}
	
}
