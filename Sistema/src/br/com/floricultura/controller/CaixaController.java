package br.com.floricultura.controller;

import java.time.LocalDate;
import java.util.Date;

import br.com.floricultura.dao.CaixaDAO;
import br.com.floricultura.model.CaixaModel;

public class CaixaController {
	
	CaixaDAO caixaDao = new CaixaDAO();
	
	public void abrirCaixa(CaixaModel caixa) {
		caixaDao.abrirCaixa(caixa);
	}
	
	public void fecharCaixa(CaixaModel caixa) {
		caixaDao.fecharCaixa(caixa);
	}
	
	public CaixaModel buscarCaixaPorData(LocalDate data) {
		return caixaDao.buscarCaixaPorData(data);
	}
}
