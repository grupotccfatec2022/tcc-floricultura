package br.com.floricultura.controller;

import br.com.floricultura.dao.LoginDAO;

public class LoginController {
	private LoginDAO loginDAO = new LoginDAO();
	
	public boolean logar(String login, String senha) {
		return loginDAO.logar(login, senha);
	}
}
