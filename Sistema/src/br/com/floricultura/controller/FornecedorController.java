package br.com.floricultura.controller;

import java.util.List;

import br.com.floricultura.dao.FornecedorDAO;
import br.com.floricultura.model.FornecedorModel;

public class FornecedorController {
	
	private FornecedorDAO fornecedorDAO = new FornecedorDAO();
	
	public boolean incluirFornecedor(FornecedorModel fornecedor) {
		return fornecedorDAO.incluirFornecedor(fornecedor);
		
	}
	
	public boolean alterarFornecedor(FornecedorModel fornecedor) {
		return fornecedorDAO.alterarFornecedor(fornecedor);
		
	}
	
	public boolean excluirFornecedor(int idFornecedor) {
		return fornecedorDAO.excluirFornecedor(idFornecedor);
	}
	
	public FornecedorModel buscarFornecedorPorId(FornecedorModel fornecedor) {
		return fornecedorDAO.buscarFornecedorPorId(fornecedor);

	}
	
	public List<FornecedorModel> listarFornecedores() {
		fornecedorDAO = new FornecedorDAO();
		return fornecedorDAO.listarFornecedores();
		
	}
}
