package br.com.floricultura.view;

import br.com.floricultura.model.FuncionarioModel;
import br.com.floricultura.model.ProdutoModel;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class VendaView extends Application {

	private static Stage stage;
	private Pane painelPrincipal;
	private Label lblTituloPagina;
	private Label lblTelaPrincipal;
	private Label lblTelaPrincipalImg;
	private Label lblCodigoProduto;
	private Label lblDescricaoProduto;
	private Label lblQuantidade;
	private Label lblValorUnitario;
	private Label lblSubTotal;
	private Label lblTotalProdutos;

	private TextField txtCodigoProduto;
	private TextField txtDescricaoProduto;
	private TextField txtQuantidade;
	private TextField txtValorUnitario;
	private TextField txtSubTotal;
	private TextField txtTotalProdutos;

	private Button btnPagar;
	private Button btnVoltar;

	private Image imgTelaPrincipal;
	
	private TableView<ProdutoModel> tabela;
	private ProdutoModel produto;

	@Override
	public void start(Stage stage) throws Exception {
		try {

			initComponents();
			initListeners();
			initLayout();
			initTable();
			Scene scene = new Scene(painelPrincipal, 1024, 600);

			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Stage getStage() {
		return stage;
	}

	private static void setStage(Stage stage) {
		VendaView.stage = stage;

	}

	private void initComponents() {
		painelPrincipal = new Pane();
		lblTelaPrincipalImg = new Label();
		lblTelaPrincipal = new Label("Tela Principal");
		lblTituloPagina = new Label("Menu Principal");

		lblCodigoProduto = new Label("C�digo Produto");
		lblDescricaoProduto = new Label("Descri��o do Produto");
		lblQuantidade = new Label("Quantidade");
		lblValorUnitario = new Label("Valor Unit.");
		lblSubTotal = new Label("Sub Total");
		lblTotalProdutos = new Label("Total");

		txtCodigoProduto = new TextField();
		txtDescricaoProduto = new TextField();
		txtQuantidade = new TextField();
		txtValorUnitario = new TextField();
		txtSubTotal = new TextField();
		txtTotalProdutos = new TextField();
		
		tabela = new TableView<>();
		tabela.setItems(getProdutos());
		
		 btnVoltar = new Button("Voltar");
		 btnPagar = new Button("Pagar");

		imgTelaPrincipal = new Image(getClass().getResourceAsStream("/imagens/testeImg.png"));
		lblTelaPrincipalImg.setGraphic(new ImageView(imgTelaPrincipal));

		painelPrincipal.getChildren().addAll(lblCodigoProduto, lblDescricaoProduto, lblQuantidade, lblValorUnitario, lblSubTotal, 
				lblTotalProdutos, txtDescricaoProduto, txtCodigoProduto, txtQuantidade, txtValorUnitario, txtSubTotal, txtTotalProdutos,
				tabela, btnVoltar, btnPagar);

	}

	private ObservableList<ProdutoModel> getProdutos() {
		// TODO Auto-generated method stub
		return null;
	}

	private void initLayout() {
		lblTituloPagina.setLayoutX(400);
		lblTituloPagina.setLayoutY(10);

		lblTelaPrincipal.setLayoutX(450);
		lblTelaPrincipal.setLayoutY(320);

		lblTelaPrincipalImg.setLayoutX(450);
		lblTelaPrincipalImg.setLayoutY(250);
		
		lblCodigoProduto.setLayoutX(20);
		lblCodigoProduto.setLayoutY(80);
		txtCodigoProduto.setLayoutX(20);
		txtCodigoProduto.setLayoutY(100);
		txtCodigoProduto.setPrefWidth(300);
		
		lblDescricaoProduto.setLayoutX(400);
		lblDescricaoProduto.setLayoutY(80);
		txtDescricaoProduto.setLayoutX(400);
		txtDescricaoProduto.setLayoutY(100);
		txtDescricaoProduto.setPrefWidth(300);
		
		lblQuantidade.setLayoutX(20);
		lblQuantidade.setLayoutY(140);
		txtQuantidade.setLayoutX(20);
		txtQuantidade.setLayoutY(160);
		txtQuantidade.setPrefWidth(100);
		
		lblValorUnitario.setLayoutX(220);
		lblValorUnitario.setLayoutY(140);
		txtValorUnitario.setLayoutX(220);
		txtValorUnitario.setLayoutY(160);
		txtValorUnitario.setPrefWidth(100);
		
		lblSubTotal.setLayoutX(400);
		lblSubTotal.setLayoutY(140);
		txtSubTotal.setLayoutX(400);
		txtSubTotal.setLayoutY(160);
		txtSubTotal.setPrefWidth(100);
		
		
		tabela.setLayoutX(20);
		tabela.setLayoutY(230);
		tabela.setPrefSize(990, 250);
		
		lblTotalProdutos.setLayoutX(910);
		lblTotalProdutos.setLayoutY(500);
		txtTotalProdutos.setLayoutX(910);
		txtTotalProdutos.setLayoutY(520);
		txtTotalProdutos.setPrefWidth(100);
		txtTotalProdutos.setDisable(true);
		

		btnVoltar.setLayoutX(370);
		btnVoltar.setLayoutY(520);
		btnVoltar.setPrefWidth(100);
		btnPagar.setLayoutX(530);
		btnPagar.setLayoutY(520);
		btnPagar.setPrefWidth(100);

	}
	
	private void initTable() {
		TableColumn<ProdutoModel, Integer> codigoColuna = new TableColumn<>("C�d. Produto");
		codigoColuna.setMinWidth(100);
		codigoColuna.setCellValueFactory(new PropertyValueFactory<>("codigoProduto"));

		TableColumn<ProdutoModel, String> descricaoColuna = new TableColumn<>("Descri��o do Produto");
		descricaoColuna.setMinWidth(200);
		descricaoColuna.setCellValueFactory(new PropertyValueFactory<>("nomeFuncionario"));

		TableColumn<ProdutoModel, Long> quantidade = new TableColumn<>("Quantidade");
		quantidade.setMinWidth(100);
		quantidade.setCellValueFactory(new PropertyValueFactory<>("quantidade"));

		TableColumn<ProdutoModel, String> valorUnitario = new TableColumn<>("Valor Unit.");
		valorUnitario.setMinWidth(130);
		valorUnitario.setCellValueFactory(new PropertyValueFactory<>("valorVenda"));

		TableColumn<ProdutoModel, String> valorTotalProdutos = new TableColumn<>("Total Produtos");
		valorTotalProdutos.setMinWidth(130);
		valorTotalProdutos.setCellValueFactory(new PropertyValueFactory<>("totalProdutos"));

		TableColumn<ProdutoModel, String> acao = new TableColumn<>("A��o");
		acao.setMinWidth(328);
		acao.setCellValueFactory(new PropertyValueFactory<>("nivelAcesso"));

		tabela.getColumns().addAll(codigoColuna, descricaoColuna, quantidade, valorUnitario, valorTotalProdutos, acao);
	}

	private void initListeners() {
		btnVoltar.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new TelaPrincipalView().start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) btnVoltar.getScene().getWindow();
				stage.close();

			}

		});

	}

	public static void main(String[] args) {
		launch(args);
	}

}
