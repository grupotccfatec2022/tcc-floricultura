package br.com.floricultura.view;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import br.com.floricultura.controller.CaixaController;
import br.com.floricultura.model.CaixaModel;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class CaixaView extends Application {

	private static Stage stage;
	private Pane painelPrincipal;
	private Label lblTituloPagina;
	private Label lblTelaPrincipal;
	private Label lblTelaPrincipalImg;

	private Label lblValorInicial;
	private TextField txtValorInicial;
	
	private Button btnAbrirCaixa;
	private Button btnFecharCaixa;


	@Override
	public void start(Stage stage) throws Exception {
		try {

			initComponents();
			initListeners();
			initLayout();

			Scene scene = new Scene(painelPrincipal, 1024, 600);

			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Stage getStage() {
		return stage;
	}

	private static void setStage(Stage stage) {
		CaixaView.stage = stage;

	}

	private void initComponents() {
		painelPrincipal = new Pane();
		lblTelaPrincipalImg = new Label();
		lblTelaPrincipal = new Label("Produto");
		lblTituloPagina = new Label("Abertura de Caixa");
		
		
		lblValorInicial = new Label("Valor R$");
		txtValorInicial = new TextField();
		btnAbrirCaixa = new Button("Abrir Caixa");
		btnFecharCaixa = new Button("Fechar Caixa");

		painelPrincipal.getChildren().addAll(lblTituloPagina, lblValorInicial,
				txtValorInicial, btnAbrirCaixa, btnFecharCaixa);

	}

	private void initLayout() {
		lblTituloPagina.setLayoutX(400);
		lblTituloPagina.setLayoutY(10);

		lblTelaPrincipal.setLayoutX(450);
		lblTelaPrincipal.setLayoutY(320);

		lblTelaPrincipalImg.setLayoutX(450);
		lblTelaPrincipalImg.setLayoutY(250);
		
		lblValorInicial.setLayoutX(400);
		lblValorInicial.setLayoutY(80);
		txtValorInicial.setLayoutX(400);
		txtValorInicial.setLayoutY(105);
		
		btnAbrirCaixa.setLayoutX(400);
		btnAbrirCaixa.setLayoutY(140);
		btnAbrirCaixa.setPrefWidth(100);
		
		btnFecharCaixa.setLayoutX(400);
		btnFecharCaixa.setLayoutY(170);
		btnFecharCaixa.setPrefWidth(100);
		

	}

	private void initListeners() {
		btnAbrirCaixa.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				
				CaixaModel caixa = new CaixaModel();
				CaixaController caixaController = new CaixaController();
				
				caixa.setValorInicial(new BigDecimal(txtValorInicial.getText()));
				caixa.setStatusCaixa("ABERTO");

				caixaController.abrirCaixa(caixa);
			}

		});
		
		
		btnFecharCaixa.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
			
				CaixaController caixaController = new CaixaController();
				CaixaModel caixa = caixaController.buscarCaixaPorData(LocalDate.now());
				caixa.setStatusCaixa("FECHADO");
				caixaController.fecharCaixa(caixa);
			}

		});

	}

	public static void main(String[] args) {
		launch(args);
	}
}