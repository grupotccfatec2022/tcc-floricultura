package br.com.floricultura.view;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class RelatorioView extends Application {

	private static Stage stage;
	private Pane painelPrincipal;
	private Label lblTituloPagina;
	private Label lblTipoRelatorio;
	private Label lblDataDe;
	private Label lblDataAte;
	private Label lblUsuario;
	private Label lblTelaPrincipal;
	private Label lblTelaPrincipalImg;
	private Image imgTelaPrincipal;

	private TextField txtDataDe;
	private TextField txtDataAte;
	private TextField txtUsuario;
	private ComboBox cmbTipo;
	
	private Button btnVoltar;

	@Override
	public void start(Stage stage) throws Exception {
		try {

			initComponents();
			initListeners();
			initLayout();

			Scene scene = new Scene(painelPrincipal, 1024, 600);

			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Stage getStage() {
		return stage;
	}

	private static void setStage(Stage stage) {
		RelatorioView.stage = stage;

	}

	private void initComponents() {
		painelPrincipal = new Pane();
		lblTelaPrincipalImg = new Label();
		lblTelaPrincipal = new Label("Tela RelatorioView");
		lblTituloPagina = new Label("Relat�rio");
		
		lblTipoRelatorio = new Label("Tipo de Relat�rio");
		lblDataDe = new Label("Data de");
		lblDataAte = new Label("Data At�");
		lblUsuario = new Label("Usu�rio");

		txtDataDe = new TextField();
		txtDataAte = new TextField();
		txtUsuario = new TextField();

		cmbTipo = new ComboBox();
		cmbTipo.getItems().addAll("Venda", "Usu�rio");
		
		btnVoltar = new Button("Voltar");

		imgTelaPrincipal = new Image(getClass().getResourceAsStream("/imagens/testeImg.png"));
		lblTelaPrincipalImg.setGraphic(new ImageView(imgTelaPrincipal));

		painelPrincipal.getChildren().addAll(lblTituloPagina, lblTipoRelatorio,
				lblDataDe, lblDataAte, lblUsuario, txtDataDe, txtDataAte, txtUsuario, cmbTipo, btnVoltar);

	}

	private void initLayout() {
		lblTituloPagina.setLayoutX(400);
		lblTituloPagina.setLayoutY(10);

		lblTelaPrincipal.setLayoutX(450);
		lblTelaPrincipal.setLayoutY(320);

		lblTelaPrincipalImg.setLayoutX(450);
		lblTelaPrincipalImg.setLayoutY(250);
		
		
		lblTipoRelatorio.setLayoutX(20);
		lblTipoRelatorio.setLayoutY(80);
		cmbTipo.setLayoutX(20);
		cmbTipo.setLayoutY(100);
		cmbTipo.setPrefWidth(250);
		
		lblDataDe.setLayoutX(350);
		lblDataDe.setLayoutY(80);
		txtDataDe.setLayoutX(350);
		txtDataDe.setLayoutY(100);
		txtDataDe.setPrefWidth(100);
		
		lblDataAte.setLayoutX(530);
		lblDataAte.setLayoutY(80);
		txtDataAte.setLayoutX(530);
		txtDataAte.setLayoutY(100);
		txtDataAte.setPrefWidth(100);
		
		lblUsuario.setLayoutX(700);
		lblUsuario.setLayoutY(80);
		txtUsuario.setLayoutX(700);
		txtUsuario.setLayoutY(100);
		txtUsuario.setPrefWidth(300);
		
		btnVoltar.setLayoutX(370);
		btnVoltar.setLayoutY(520);
		btnVoltar.setPrefWidth(100);

	}

	private void initListeners() {
		btnVoltar.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new TelaPrincipalView().start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) btnVoltar.getScene().getWindow();
				stage.close();

			}

		});

	}

	public static void main(String[] args) {
		launch(args);
	}
}
