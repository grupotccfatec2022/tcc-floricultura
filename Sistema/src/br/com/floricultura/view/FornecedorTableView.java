package br.com.floricultura.view;

import br.com.floricultura.controller.FornecedorController;
import br.com.floricultura.model.FornecedorModel;
import br.com.floricultura.model.FuncionarioModel;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

public class FornecedorTableView extends Application {

	private static Stage stage;
	private Pane painelPrincipal;
	private Label lblTituloPagina;
	private Label lblTelaPrincipal;
	private Label lblTelaPrincipalImg;
	private Image imgTelaPrincipal;

	private Label lblCpfCnpj;
	private TextField txtCpfCnpj;
	private Button btnVoltar;
	private Label lblSairImg;
	private Label lblMenuImg;
	private Image imgSair;
	private Image imgMenu;

	private TableView<FornecedorModel> tabela;

	private FornecedorModel fornecedor;

	ObservableList<FornecedorModel> fornecedores = FXCollections.observableArrayList();
	FornecedorController controller = new FornecedorController();

	@Override
	public void start(Stage stage) throws Exception {
		try {

			initComponents();
			initListeners();
			initLayout();
			initTable();
			Scene scene = new Scene(painelPrincipal, 1024, 600);

			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Stage getStage() {
		return stage;
	}

	private static void setStage(Stage stage) {
		TesteFuncionario.stage = stage;

	}

	private void initComponents() {
		painelPrincipal = new Pane();
		lblTelaPrincipalImg = new Label();
		lblTituloPagina = new Label("Funcionario");

		lblCpfCnpj = new Label("CPF/CNPJ");

		txtCpfCnpj = new TextField();

		tabela = new TableView<>();
		tabela.setItems(getFornecedores());

		btnVoltar = new Button("Voltar");

		imgTelaPrincipal = new Image(getClass().getResourceAsStream("/imagens/testeImg.png"));
		lblTelaPrincipalImg.setGraphic(new ImageView(imgTelaPrincipal));

		painelPrincipal.getChildren().addAll(tabela, lblCpfCnpj, txtCpfCnpj, btnVoltar);

	}

	private void initLayout() {
		lblTituloPagina.setLayoutX(500);
		lblTituloPagina.setLayoutY(10);

		lblCpfCnpj.setLayoutX(350);
		lblCpfCnpj.setLayoutY(80);
		txtCpfCnpj.setLayoutX(350);
		txtCpfCnpj.setLayoutY(100);
		txtCpfCnpj.setPrefWidth(300);

		btnVoltar.setLayoutX(800);
		btnVoltar.setLayoutY(550);
		btnVoltar.setPrefWidth(100);

		tabela.setLayoutX(20);
		tabela.setLayoutY(200);
		tabela.setPrefSize(990, 250);

	}

	private void initListeners() {
		btnVoltar.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new TelaPrincipalView().start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) btnVoltar.getScene().getWindow();
				stage.close();

			}

		});

	}

	private void initTable() {
		TableColumn<FornecedorModel, Integer> idColuna = new TableColumn<>("C�digo");
		idColuna.setMinWidth(50);
		idColuna.setCellValueFactory(new PropertyValueFactory<>("codFornecedor"));

		TableColumn<FornecedorModel, String> razaoSocialColuna = new TableColumn<>("Raz�o Social");
		razaoSocialColuna.setMinWidth(150);
		razaoSocialColuna.setCellValueFactory(new PropertyValueFactory<>("razaoSocial"));
		
		TableColumn<FornecedorModel, String> nomeFantasiaColuna = new TableColumn<>("Nome Fantasia");
		nomeFantasiaColuna.setMinWidth(150);
		nomeFantasiaColuna.setCellValueFactory(new PropertyValueFactory<>("nomeFantasia"));

		TableColumn<FornecedorModel, Long> cpfCnpj = new TableColumn<>("CPF/CNPJ");
		cpfCnpj.setMinWidth(100);
		cpfCnpj.setCellValueFactory(new PropertyValueFactory<>("cpfCnpj"));

		TableColumn<FornecedorModel, String> tipoEmpresaColuna = new TableColumn<>("Tipo Empresa");
		tipoEmpresaColuna.setMinWidth(100);
		tipoEmpresaColuna.setCellValueFactory(new PropertyValueFactory<>("tipoEmpresa"));
		
		TableColumn<FornecedorModel, String> logradouroColuna = new TableColumn<>("Lougradouro");
		logradouroColuna.setMinWidth(150);
		logradouroColuna.setCellValueFactory(new PropertyValueFactory<>("nomeFantasia"));
		
		TableColumn<FornecedorModel, String> numeroColuna = new TableColumn<>("N�");
		numeroColuna.setMinWidth(50);
		numeroColuna.setCellValueFactory(new PropertyValueFactory<>("nomeFantasia"));
		
		TableColumn<FornecedorModel, String> bairroColuna = new TableColumn<>("Bairro");
		bairroColuna.setMinWidth(100);
		bairroColuna.setCellValueFactory(new PropertyValueFactory<>("nomeFantasia"));
		
		TableColumn<FornecedorModel, String> cidadeColuna = new TableColumn<>("Cidade");
		cidadeColuna.setMinWidth(100);
		cidadeColuna.setCellValueFactory(new PropertyValueFactory<>("nomeFantasia"));
		
	
		TableColumn<FornecedorModel, Void> colBtn = new TableColumn("A��o");

		Callback<TableColumn<FornecedorModel, Void>, TableCell<FornecedorModel, Void>> cellFactory = new Callback<TableColumn<FornecedorModel, Void>, TableCell<FornecedorModel, Void>>() {
			@Override
			public TableCell<FornecedorModel, Void> call(final TableColumn<FornecedorModel, Void> param) {
				return new TableCell<FornecedorModel, Void>() {

					private final Button btn = new Button("Visualizar");

					{
						btn.setOnAction((ActionEvent event) -> {
							FornecedorModel data = getTableView().getItems().get(getIndex());
							// Setar os dados aqui para ir pra tela FornecedorView()
							try {
								new FornecedorView().start(new Stage());
							} catch (Exception e) {
								e.printStackTrace();
							}
							Stage stage = (Stage) btn.getScene().getWindow();
							stage.close();

							/*
							 * Alert alert = new Alert(AlertType.INFORMATION); alert.setTitle("Pagamento");
							 * alert.setHeaderText("Pagamento Parcela");
							 * alert.setContentText("referencia ID = " + data.getCodFornecedor() );
							 * 
							 * alert.showAndWait();
							 */
						});
					}

					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							FornecedorModel data = getTableView().getItems().get(getIndex());
							// btn.setText(btn.getText() + " " + data.getCodFornecedor());
							setGraphic(btn);
						}
					}
				};
			}
		};

		colBtn.setCellFactory(cellFactory);
		colBtn.setPrefWidth(128);

		FilteredList<FornecedorModel> dadosFiltrados = new FilteredList<>(fornecedores, f -> true);

		txtCpfCnpj.textProperty().addListener((observable, oldValue, newValue) -> {

			dadosFiltrados.setPredicate(fornecedor -> {

				if (newValue == null || newValue.isEmpty()) {
					return true;
				}

				String lowerCaseFilter = newValue.toLowerCase();
				if (fornecedor.getRazaoSocial().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				//} else if (fornecedor.getNomeFantasia().toLowerCase().indexOf(lowerCaseFilter) != -1) {
				//	return true;
				} else if (String.valueOf(fornecedor.getCpfCnpj()).indexOf(newValue) != -1)
					return true;
				else
					return false;
			});
		});

		SortedList<FornecedorModel> sortedData = new SortedList<>(dadosFiltrados);

		sortedData.comparatorProperty().bind(tabela.comparatorProperty());

		tabela.getColumns().addAll(idColuna, cpfCnpj, razaoSocialColuna,  nomeFantasiaColuna, 
				logradouroColuna, numeroColuna, bairroColuna, cidadeColuna, colBtn);

		tabela.setItems(sortedData);
	}

	public ObservableList<FornecedorModel> getFornecedores() {

		for (FornecedorModel fornecedor : controller.listarFornecedores()) {
			fornecedor.getCodFornecedor();
			fornecedor.getRazaoSocial();
			fornecedor.getNomeFantasia();
			fornecedor.getCpfCnpj();

			fornecedores.addAll(fornecedor);
		}

		return fornecedores;

	}

	public static void main(String[] args) {
		launch(args);
	}
}
