package br.com.floricultura.view;

import br.com.floricultura.model.FuncionarioModel;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class TesteFuncionario extends Application {

	static Stage stage;
	private Pane painelPrincipal;
	private Label lblTituloPagina;
	private Label lblTelaPrincipal;
	private Label lblTelaPrincipalImg;
	private Image imgTelaPrincipal;

	private Label lblNome;
	private Label lblCPF;
	private Label lblDataNascimento;
	private Label lblLougradouro;
	private Label lblNumero;
	private Label lblCEP;
	private Label lblCidade;
	private Label lblUF;
	private Label lblBairro;
	private Label lblComplemento;
	private Label lblCelular;
	private Label lblTelefone;
	private Label lblDataAdmissao;
	private Label lblCargo;
	private Label lblEmail;
	private Label lblSenha;
	private Label lblSenha2;

	private Label lblSairImg;
	private Label lblMenuImg;
	private Image imgSair;
	private Image imgMenu;

	private TextField txtNome;
	private TextField txtCPF;
	private TextField DataNascimento;
	private TextField txtCEP;
	private TextField txtCidade;
	private TextField txtUF;
	private TextField txtBairro;
	private TextField txtLogradouro;
	private TextField txtComplemento;
	private TextField txtNumero;
	private TextField txtCelular;
	private TextField txtTelefone;
	private TextField DataAdmissao;
	private ComboBox cmbCargo;
	private TextField txtEmail;
	private PasswordField txtSenha;
	private PasswordField txtSenha2;
	
	private Button btnSalvar;
	private Button btnCancelar;
	private Button btnExcluir;
	private Button btnVoltar;
	
	private TableView<FuncionarioModel> tabela;

	private FuncionarioModel funcionario;
	
	@Override
	public void start(Stage stage) throws Exception {
		try {

			initComponents();
			initListeners();
			initLayout();
			initTable();
			Scene scene = new Scene(painelPrincipal, 1024, 600);

			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Stage getStage() {
		return stage;
	}

	private static void setStage(Stage stage) {
		TesteFuncionario.stage = stage;

	}

	private void initComponents() {
		painelPrincipal = new Pane();
		lblTelaPrincipalImg = new Label();
		lblTituloPagina = new Label("Funcionario");

		lblNome = new Label("Nome");
		lblCPF = new Label("CPF");
		lblDataNascimento = new Label("Data de Nascimento");
		lblCEP = new Label("CEP");
		lblLougradouro = new Label("Lougradouro");
		lblNumero = new Label("N�");
		lblComplemento = new Label("Complemento");
		lblBairro = new Label("Bairro");
		lblCidade = new Label("Cidade");
		lblUF = new Label("UF");
		lblEmail = new Label("E-mail");
		lblCelular = new Label("Celular");
		lblTelefone = new Label("Telefone");
		lblDataAdmissao = new Label("Data Admiss�o");
		lblCargo = new Label("Cargo");
		lblSenha = new Label("Senha");
		lblSenha2 = new Label("Confirma Senha");
		txtNome = new TextField();
		txtCPF = new TextField();
		DataNascimento = new TextField();
		txtCEP = new TextField();
		txtCidade = new TextField();
		txtComplemento = new TextField();
		txtUF = new TextField();
		txtBairro = new TextField();
		txtLogradouro = new TextField();
		txtNumero = new TextField();
		txtTelefone = new TextField();
		txtCelular = new TextField();
		DataAdmissao = new TextField();
		txtEmail = new TextField();
		txtSenha = new PasswordField();
		txtSenha2 = new PasswordField();

		cmbCargo = new ComboBox();
		cmbCargo.getItems().addAll("Caixa", "Gerente", "Vendedor");
		
		tabela = new TableView<>();
		tabela.setItems(getFuncionarios());
		
		btnSalvar = new Button("Salvar");
		btnCancelar = new Button("Cancelar");
		btnExcluir = new Button("Excluir");
		btnVoltar = new Button("Voltar");
		
		imgTelaPrincipal = new Image(getClass().getResourceAsStream("/imagens/testeImg.png"));
		lblTelaPrincipalImg.setGraphic(new ImageView(imgTelaPrincipal));

		painelPrincipal.getChildren().addAll(tabela, btnSalvar, btnCancelar , btnExcluir, btnVoltar);

	}

	private void initLayout() {
		lblTituloPagina.setLayoutX(500);
		lblTituloPagina.setLayoutY(10);

		lblNome.setLayoutX(20);
		lblNome.setLayoutY(80);
		txtNome.setLayoutX(20);
		txtNome.setLayoutY(100);
		txtNome.setPrefWidth(400);
		
		lblCPF.setLayoutX(500);
		lblCPF.setLayoutY(80);
		txtCPF.setLayoutX(500);
		txtCPF.setLayoutY(100);
		txtCPF.setPrefWidth(200);
		
		lblDataNascimento.setLayoutX(775);
		lblDataNascimento.setLayoutY(80);
		DataNascimento.setLayoutX(775);
		DataNascimento.setLayoutY(100);
		DataNascimento.setPrefWidth(200);
		
		lblCEP.setLayoutX(20);
		lblCEP.setLayoutY(140);
		txtCEP.setLayoutX(20);
		txtCEP.setLayoutY(160);
		txtCEP.setPrefWidth(100);
		
		lblLougradouro.setLayoutX(200);
		lblNumero.setLayoutY(140);
		txtLogradouro.setLayoutX(200);
		txtLogradouro.setLayoutY(160);
		txtLogradouro.setPrefWidth(400);
		
		lblNumero.setLayoutX(650);
		lblLougradouro.setLayoutY(140);
		txtNumero.setLayoutX(650);
		txtNumero.setLayoutY(160);
		txtNumero.setPrefWidth(50);
		
		lblComplemento.setLayoutX(20);
		lblComplemento.setLayoutY(200);
		txtComplemento.setLayoutX(20);
		txtComplemento.setLayoutY(220);
		txtComplemento.setPrefWidth(300);
		
		lblBairro.setLayoutX(400);
		lblBairro.setLayoutY(200);
		txtBairro.setLayoutX(400);
		txtBairro.setLayoutY(220);
		txtBairro.setPrefWidth(200);
		
		lblCidade.setLayoutX(650);
		lblCidade.setLayoutY(200);
		txtCidade.setLayoutX(650);
		txtCidade.setLayoutY(220);
		txtCidade.setPrefWidth(200);
		
		lblUF.setLayoutX(923);
		lblUF.setLayoutY(200);
		txtUF.setLayoutX(923);
		txtUF.setLayoutY(220);
		txtUF.setPrefWidth(50);
		
		lblEmail.setLayoutX(20);
		lblEmail.setLayoutY(260);
		txtEmail.setLayoutX(20);
		txtEmail.setLayoutY(280);
		txtEmail.setPrefWidth(400);
		
		lblCelular.setLayoutX(500);
		lblCelular.setLayoutY(260);
		txtCelular.setLayoutX(500);
		txtCelular.setLayoutY(280);
		txtCelular.setPrefWidth(200);
		
		lblTelefone.setLayoutX(775);
		lblTelefone.setLayoutY(260);
		txtTelefone.setLayoutX(775);
		txtTelefone.setLayoutY(280);
		txtTelefone.setPrefWidth(200);
		
		lblCargo.setLayoutX(20);
		lblCargo.setLayoutY(320);
		cmbCargo.setLayoutX(20);
		cmbCargo.setLayoutY(340);
		cmbCargo.setPrefWidth(120);
		
		lblSenha.setLayoutX(220);
		lblSenha.setLayoutY(320);
		txtSenha.setLayoutX(220);
		txtSenha.setLayoutY(340);
		txtSenha.setPrefWidth(200);
		
		lblSenha2.setLayoutX(500);
		lblSenha2.setLayoutY(320);
		txtSenha2.setLayoutX(500);
		txtSenha2.setLayoutY(340);
		txtSenha2.setPrefWidth(200);
		
		lblDataAdmissao.setLayoutX(775);
		lblDataAdmissao.setLayoutY(320);
		DataAdmissao.setLayoutX(775);
		DataAdmissao.setLayoutY(340);
		DataAdmissao.setPrefWidth(200);
		
		btnSalvar.setLayoutX(200);
		btnSalvar.setLayoutY(550);
		btnSalvar.setPrefWidth(100);
		
		btnCancelar.setLayoutX(400);
		btnCancelar.setLayoutY(550);
		btnCancelar.setPrefWidth(100);
		
		btnExcluir.setLayoutX(600);
		btnExcluir.setLayoutY(550);
		btnExcluir.setPrefWidth(100);
		
		btnVoltar.setLayoutX(800);
		btnVoltar.setLayoutY(550);
		btnVoltar.setPrefWidth(100);
		
		tabela.setLayoutX(20);
		tabela.setLayoutY(200);
		tabela.setPrefSize(990, 250);
		
	}

	private void initListeners() {
		btnVoltar.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new TelaPrincipalView().start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) btnVoltar.getScene().getWindow();
				stage.close();

			}

		});

	}
	
	private void initTable() {
		TableColumn<FuncionarioModel, Integer> idColuna = new TableColumn<>("Codigo");
		idColuna.setMinWidth(50);
		idColuna.setCellValueFactory(new PropertyValueFactory<>("codigoFuncionario"));

		TableColumn<FuncionarioModel, String> nomeColuna = new TableColumn<>("Nome");
		nomeColuna.setMinWidth(200);
		nomeColuna.setCellValueFactory(new PropertyValueFactory<>("nomeFuncionario"));

		TableColumn<FuncionarioModel, Long> cpf = new TableColumn<>("CPF");
		cpf.setMinWidth(100);
		cpf.setCellValueFactory(new PropertyValueFactory<>("cpf"));

		TableColumn<FuncionarioModel, String> email = new TableColumn<>("E-mail");
		email.setMinWidth(200);
		email.setCellValueFactory(new PropertyValueFactory<>("emailFuncionario"));

		TableColumn<FuncionarioModel, String> senha = new TableColumn<>("Senha");
		senha.setMinWidth(100);
		senha.setCellValueFactory(new PropertyValueFactory<>("senha"));

		TableColumn<FuncionarioModel, String> nivelAcesso = new TableColumn<>("Nivel Acesso");
		nivelAcesso.setMinWidth(100);
		nivelAcesso.setCellValueFactory(new PropertyValueFactory<>("nivelAcesso"));

		tabela.getColumns().addAll(idColuna, nomeColuna, cpf, email, senha, nivelAcesso);
	}
	
	public ObservableList<FuncionarioModel> getFuncionarios() {
		ObservableList<FuncionarioModel> funcionarios = FXCollections.observableArrayList();

		/*FuncionarioController funcionarioMod = new FuncionarioController();
		for (FuncionarioModel f : funcionarioMod.listaFuncionario()) {
			f.getCodigoFuncionario();
			f.getNomeFuncionario();
			f.getCpf();
			f.getEmailFuncionario();
			f.getSenha();
			f.getNivelAcesso();

			funcionarios.add(f);
		}
*/
		return funcionarios;

	}


	public static void main(String[] args) {
		launch(args);
	}
}

