package br.com.floricultura.view;

import java.time.LocalDate;

import br.com.floricultura.controller.FornecedorController;
import br.com.floricultura.model.ContatoModel;
import br.com.floricultura.model.EnderecoModel;
import br.com.floricultura.model.FornecedorModel;
import br.com.floricultura.utility.MensagensAlerta;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class FornecedorView extends Application {

	static Stage stage;
	private Pane painelPrincipal;
	private Label lblTituloPagina;
	private Label lblTelaPrincipal;
	private Label lblTelaPrincipalImg;
	private Image imgTelaPrincipal;

	private Label lblRazaoSocial;
	private Label lblNomeFantasia;
	private Label lblInscEstadual;
	private Label lblInscMunicipal;
	private Label lblCpfCnpj;
	private Label lblLogradouro;
	private Label lblNumero;
	private Label lblCEP;
	private Label lblCidade;
	private Label lblUF;
	private Label lblBairro;
	private Label lblComplemento;
	private Label lblCelular;
	private Label lblTelefone;
	private Label lblEmail;
	private Label lblDataInclusaoSistema;
	private Label lblTipoEmpresa;

	private Label lblSairImg;
	private Label lblMenuImg;
	private Image imgSair;
	private Image imgMenu;

	private TextField txtRazaoSocial;
	private TextField txtNomeFantasia;
	private TextField txtInscEstadual;
	private TextField txtInscMunicipal;
	private TextField txtCpfCnpj;
	private TextField txtCEP;
	private TextField txtCidade;
	private TextField txtUF;
	private TextField txtBairro;
	private TextField txtLogradouro;
	private TextField txtComplemento;
	private TextField txtNumero;
	private TextField txtCelular;
	private TextField txtTelefone;
	private DatePicker dataInclusaoSistema;
	private TextField txtEmail;
	private ComboBox cmbEmpresa;
	
	private Button btnSalvar;
	private Button btnAlterar;
	private Button btnCancelar;
	private Button btnExcluir;
	private Button btnVoltar;
	
	private FornecedorController fornecedorController;
	private MensagensAlerta msgAlerta;
	
	
	@Override
	public void start(Stage stage) throws Exception {
		try {

			initComponents();
			initListeners();
			initLayout();
			
			//setCampos();
			
			Scene scene = new Scene(painelPrincipal, 1024, 600);

			stage.setScene(scene);
			stage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Stage getStage() {
		return stage;
	}

	private static void setStage(Stage stage) {
		FuncionarioView.stage = stage;

	}

	private void initComponents() {
		painelPrincipal = new Pane();
		lblTelaPrincipalImg = new Label();
		lblTituloPagina = new Label("Fornecedor");

		lblRazaoSocial = new Label("Raz�o Social");
		lblNomeFantasia= new Label("Nome Fantasia");
		lblCpfCnpj = new Label("CPF/CNPJ");
		lblInscEstadual = new Label("Insc. Estadual");
		lblInscMunicipal = new Label("Insc. Municipal");
		lblCEP = new Label("CEP");
		lblLogradouro = new Label("Logradouro");
		lblNumero = new Label("N�");
		lblComplemento = new Label("Complemento");
		lblBairro = new Label("Bairro");
		lblCidade = new Label("Cidade");
		lblUF = new Label("UF");
		lblEmail = new Label("E-mail");
		lblCelular = new Label("Celular");
		lblTelefone = new Label("Telefone");
		lblDataInclusaoSistema = new Label("Data Inclus�o no Sistema");
		lblTipoEmpresa = new Label("Tipo Empresa");
	
		txtRazaoSocial = new TextField();
		txtNomeFantasia = new TextField();
		txtCpfCnpj = new TextField();
		txtInscEstadual = new TextField();
		txtInscMunicipal = new TextField();
		txtCEP = new TextField();
		txtCidade = new TextField();
		txtComplemento = new TextField();
		txtUF = new TextField();
		txtBairro = new TextField();
		txtLogradouro = new TextField();
		txtNumero = new TextField();
		txtTelefone = new TextField();
		txtCelular = new TextField();
		txtEmail = new TextField();
	
		dataInclusaoSistema = new DatePicker();
		
		cmbEmpresa = new ComboBox();
		cmbEmpresa.getItems().addAll("Sede", "Filial");
		
		btnSalvar = new Button("Salvar");
		btnAlterar= new Button("Alterar");
		btnCancelar = new Button("Cancelar");
		btnExcluir = new Button("Excluir");
		btnVoltar = new Button("Voltar");
		
		imgTelaPrincipal = new Image(getClass().getResourceAsStream("/imagens/testeImg.png"));
		lblTelaPrincipalImg.setGraphic(new ImageView(imgTelaPrincipal));

		painelPrincipal.getChildren().addAll(lblTituloPagina, lblRazaoSocial, lblNomeFantasia,
				lblCEP, lblCidade, lblUF, lblBairro, lblLogradouro, lblNumero, lblComplemento, lblCelular,
				lblTelefone, lblEmail, lblInscEstadual,
				lblInscMunicipal,
				lblDataInclusaoSistema,
				lblCpfCnpj,
				cmbEmpresa,
				lblTipoEmpresa,
				txtCpfCnpj,
				txtRazaoSocial,
				txtNomeFantasia,
				dataInclusaoSistema,
				txtInscEstadual,
				txtInscMunicipal,
				txtCEP, txtCidade, txtUF, txtBairro, txtComplemento, txtLogradouro, txtNumero, txtTelefone, txtCelular, txtEmail,
				 btnSalvar,btnAlterar, btnCancelar , btnExcluir, btnVoltar);

	}

	private void initLayout() {
		lblTituloPagina.setLayoutX(500);
		lblTituloPagina.setLayoutY(10);
	
		lblRazaoSocial.setLayoutX(20);
		lblRazaoSocial.setLayoutY(80);
		txtRazaoSocial.setLayoutX(20);
		txtRazaoSocial.setLayoutY(100);
		txtRazaoSocial.setPrefWidth(400);
		
		lblNomeFantasia.setLayoutX(500);
		lblNomeFantasia.setLayoutY(80);
		txtNomeFantasia.setLayoutX(500);
		txtNomeFantasia.setLayoutY(100);
		txtNomeFantasia.setPrefWidth(400);
		
		lblCpfCnpj.setLayoutX(20);
		lblCpfCnpj.setLayoutY(140);
		txtCpfCnpj.setLayoutX(20);
		txtCpfCnpj.setLayoutY(160);
		txtCpfCnpj.setPrefWidth(200);
		
		lblInscEstadual.setLayoutX(300);
		lblInscEstadual.setLayoutY(140);
		txtInscEstadual.setLayoutX(300);
		txtInscEstadual.setLayoutY(160);
		txtInscEstadual.setPrefWidth(200);
		
		lblInscMunicipal.setLayoutX(575);
		lblInscMunicipal.setLayoutY(140);
		txtInscMunicipal.setLayoutX(575);
		txtInscMunicipal.setLayoutY(160);
		txtInscMunicipal.setPrefWidth(200);
	
		lblDataInclusaoSistema.setLayoutX(825);
		lblDataInclusaoSistema.setLayoutY(140);
		dataInclusaoSistema.setLayoutX(825);
		dataInclusaoSistema.setLayoutY(160);
		dataInclusaoSistema.setPrefWidth(150);
		
		lblCEP.setLayoutX(20);
		lblCEP.setLayoutY(200);
		txtCEP.setLayoutX(20);
		txtCEP.setLayoutY(220);
		txtCEP.setPrefWidth(100);
		
		lblLogradouro.setLayoutX(200);
		lblLogradouro.setLayoutY(200);
		txtLogradouro.setLayoutX(200);
		txtLogradouro.setLayoutY(220);
		txtLogradouro.setPrefWidth(400);
		
		lblNumero.setLayoutX(650);
		lblNumero.setLayoutY(200);
		txtNumero.setLayoutX(650);
		txtNumero.setLayoutY(220);
		txtNumero.setPrefWidth(50);
		
		lblComplemento.setLayoutX(20);
		lblComplemento.setLayoutY(260);
		txtComplemento.setLayoutX(20);
		txtComplemento.setLayoutY(280);
		txtComplemento.setPrefWidth(300);
		
		lblBairro.setLayoutX(400);
		lblBairro.setLayoutY(260);
		txtBairro.setLayoutX(400);
		txtBairro.setLayoutY(280);
		txtBairro.setPrefWidth(200);
		
		lblCidade.setLayoutX(650);
		lblCidade.setLayoutY(260);
		txtCidade.setLayoutX(650);
		txtCidade.setLayoutY(280);
		txtCidade.setPrefWidth(200);
		
		lblUF.setLayoutX(923);
		lblUF.setLayoutY(260);
		txtUF.setLayoutX(923);
		txtUF.setLayoutY(280);
		txtUF.setPrefWidth(50);
		
		
		lblEmail.setLayoutX(20);
		lblEmail.setLayoutY(320);
		txtEmail.setLayoutX(20);
		txtEmail.setLayoutY(340);
		txtEmail.setPrefWidth(400);
		
		lblCelular.setLayoutX(500);
		lblCelular.setLayoutY(320);
		txtCelular.setLayoutX(500);
		txtCelular.setLayoutY(340);
		txtCelular.setPrefWidth(200);
		
		lblTelefone.setLayoutX(775);
		lblTelefone.setLayoutY(320);
		txtTelefone.setLayoutX(775);
		txtTelefone.setLayoutY(340);
		txtTelefone.setPrefWidth(200);
		
		lblTelefone.setLayoutX(775);
		lblTelefone.setLayoutY(320);
		txtTelefone.setLayoutX(775);
		txtTelefone.setLayoutY(340);
		txtTelefone.setPrefWidth(200);
		
		
		lblTipoEmpresa.setLayoutX(20);
		lblTipoEmpresa.setLayoutY(380);
		cmbEmpresa.setLayoutX(20);
		cmbEmpresa.setLayoutY(400);
		cmbEmpresa.setPrefWidth(200);
		
		btnSalvar.setLayoutX(200);
		btnSalvar.setLayoutY(550);
		btnSalvar.setPrefWidth(100);
		
		btnAlterar.setLayoutX(200);
		btnAlterar.setLayoutY(500);
		btnAlterar.setPrefWidth(100);
		
		btnCancelar.setLayoutX(400);
		btnCancelar.setLayoutY(550);
		btnCancelar.setPrefWidth(100);
		
		btnExcluir.setLayoutX(600);
		btnExcluir.setLayoutY(550);
		btnExcluir.setPrefWidth(100);
		
		btnVoltar.setLayoutX(800);
		btnVoltar.setLayoutY(550);
		btnVoltar.setPrefWidth(100);
		
		
		
	}

	private void initListeners() {
		
		
		btnSalvar.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				msgAlerta = new MensagensAlerta();
				
				fornecedorController = new FornecedorController();
				
				var fornecedor = testeInclusao();
				
				/*
				FornecedorModel fornecedor = new FornecedorModel();
				EnderecoModel endereco = new EnderecoModel();
				ContatoModel contato = new ContatoModel();
				
				fornecedor.setRazaoSocial(txtRazaoSocial.getText());
				fornecedor.setNomeFantasia(txtNomeFantasia.getText());
				fornecedor.setCpfCnpj(txtCpfCnpj.getText());
				fornecedor.setInscEstadual(txtInscEstadual.getText());
				fornecedor.setInscMunicipal(txtInscMunicipal.getText());
				fornecedor.setTipoEmpresa(cmbEmpresa.getPromptText());
				
				endereco.setCep(txtCEP.getText());
				endereco.setLogradouro(txtLogradouro.getText());
				endereco.setNumero(Integer.parseInt(txtNumero.getText()));
				endereco.setComplemento(txtComplemento.getText());
				endereco.setBairro(txtBairro.getText());
				endereco.setCidade(txtCidade.getText());
				endereco.setUf(txtUF.getText());
				
				contato.setCelular(txtCelular.getText());
				contato.setTelefone(txtTelefone.getText());
				contato.setEmail(txtEmail.getText());
				
				fornecedor.setEndereco(endereco);
				fornecedor.setContato(contato);*/
				System.out.println(fornecedor);
				if(fornecedorController.incluirFornecedor(fornecedor)) {
					msgAlerta.mensagemInformativa("Teste Inclusao", null, "Sucesso");
				}
				
			}

		});
		
		
		btnExcluir.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				msgAlerta = new MensagensAlerta();
				
				fornecedorController = new FornecedorController();
				
				//var fornecedor = testeInclusao();
		
				
				if(fornecedorController.excluirFornecedor(1)) {
					msgAlerta.mensagemInformativa("Teste Exclusao", null, "Sucesso");
				}
				
			}

		});
		
		btnAlterar.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				msgAlerta = new MensagensAlerta();
				
				fornecedorController = new FornecedorController();
				
				var fornecedor = testeInclusao();
		
				fornecedor.setCpfCnpj("89354838000197");
				fornecedor.setRazaoSocial("Razao Social Teste Alteracao");
				fornecedor.setCodFornecedor(4);
				if(fornecedorController.alterarFornecedor(fornecedor)) {
					msgAlerta.mensagemInformativa("Teste Alteracao", null, "Sucesso");
				}
				
			}

		});
		
		
		
		btnVoltar.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new TelaPrincipalView().start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) btnVoltar.getScene().getWindow();
				stage.close();

			}

		});

	}
	
	private FornecedorModel testeInclusao() {
		FornecedorModel fornecedor = new FornecedorModel();
		EnderecoModel endereco = new EnderecoModel();
		ContatoModel contato = new ContatoModel();
		
		fornecedor.setRazaoSocial("Razao Social Teste 03");
		fornecedor.setNomeFantasia("Nome Fantasia Teste 03");
		fornecedor.setCpfCnpj("49039256000100");
		fornecedor.setInscEstadual("743460222677");
		fornecedor.setInscMunicipal("288363791714");
		fornecedor.setTipoEmpresa("Sede");
		fornecedor.setDataInclusao(LocalDate.now());
		endereco.setCep("19025568");
		endereco.setLogradouro("Rua Vict�rio Furlanetto");
		endereco.setNumero(5);
		endereco.setComplemento("");
		endereco.setBairro("Jardim Santa Olga");
		endereco.setCidade("Presidente Prudente");
		endereco.setUf("SP");
		
		contato.setCelular("11912345675");
		contato.setTelefone("1123456789");
		contato.setEmail("teste@teste.com");
		
		fornecedor.setEndereco(endereco);
		fornecedor.setContato(contato);
		
		return fornecedor;
		
	}
	
	public void setCampos() {
		/*RECEBER AQUI OS DADOS VINDO DA FORNECEDORTABLEVIEW E SETAR NOS CAMPOS*/
		
		/*txtCodigo.setText(String.valueOf(fornecedor.getFornecedor().getCpfCnpj()));
		txtRazaoSocial.setText(fornecedor.getFornecedor().getRazaoSocial());
		txtCpfCnpj.setText(fornecedor.getFornecedor().getCpfCnpj());
		txtNumero.setText(String.valueOf(fornecedor.getFornecedor().getCodFornecedor()));*/
		
	}

	public static void main(String[] args) {
		launch(args);
	}
}
