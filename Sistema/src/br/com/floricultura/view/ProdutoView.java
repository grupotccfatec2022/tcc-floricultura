package br.com.floricultura.view;

import javax.swing.JTextField;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ProdutoView extends Application {

	private static Stage stage;
	private Pane painelPrincipal;
	private Label lblTituloPagina;
	private Label lblTelaPrincipal;
	private Label lblTelaPrincipalImg;

	private Label lblCodigoProduto;
	private Label lblDescricaoProduto;
	private Label lblQuantidade;
	private Label lblPrecoCompra;
	private Label lblPrecoVenda;
	private Label lblCategoria;
	private Label lblValidade;
	private Label lblFornecedor;
	private Image imgTelaPrincipal;

	private TextField txtCodigoProduto;
	private TextField txtDescricaoProduto;
	private TextField txtQuantidade;
	private TextField txtPrecoCompra;
	private TextField txtPrecoVenda;
	private TextField Validade;

	private ComboBox cmbCategoria;
	private ComboBox cmbFornecedor;
	
	private Button btnSalvar;
	private Button btnCancelar;
	private Button btnExcluir;
	private Button btnVoltar;

	@Override
	public void start(Stage stage) throws Exception {
		try {

			initComponents();
			initListeners();
			initLayout();

			Scene scene = new Scene(painelPrincipal, 1024, 600);

			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Stage getStage() {
		return stage;
	}

	private static void setStage(Stage stage) {
		ProdutoView.stage = stage;

	}

	private void initComponents() {
		painelPrincipal = new Pane();
		lblTelaPrincipalImg = new Label();
		lblTelaPrincipal = new Label("Produto");
		lblTituloPagina = new Label("Produto");

		lblCodigoProduto = new Label("C�digo do Produto");
		lblDescricaoProduto = new Label("Descri��o do Produto");
		lblQuantidade = new Label("Quantidade");
		lblPrecoCompra = new Label("Pre�o de Compra");
		lblPrecoVenda = new Label("Pre�o de Venda");
		lblCategoria = new Label("Categoria");
		lblValidade = new Label("Validade");
		lblFornecedor = new Label("Fornecedor");

		txtCodigoProduto = new TextField();
		txtDescricaoProduto = new TextField();
		txtQuantidade = new TextField();
		txtPrecoCompra = new TextField();
		txtPrecoVenda = new TextField();
		Validade = new TextField();

		cmbCategoria = new ComboBox();
		cmbFornecedor = new ComboBox();
		cmbCategoria.getItems().addAll("Categoria 1", "Categoria 2", "Categoria 3");
		cmbFornecedor.getItems().addAll("Fornecedor 1", "Fornecedor 2", "Fornecedor 3");
		
		btnSalvar = new Button("Salvar");
		btnCancelar = new Button("Cancelar");
		btnExcluir = new Button("Excluir");
		btnVoltar = new Button("Voltar");

		imgTelaPrincipal = new Image(getClass().getResourceAsStream("/imagens/testeImg.png"));
		lblTelaPrincipalImg.setGraphic(new ImageView(imgTelaPrincipal));

		painelPrincipal.getChildren().addAll(lblTituloPagina, lblCodigoProduto,
				lblDescricaoProduto, lblQuantidade, lblPrecoCompra, lblPrecoVenda, lblCategoria, lblValidade,
				lblFornecedor, txtCodigoProduto, txtDescricaoProduto, txtQuantidade, txtPrecoCompra, txtPrecoVenda,
				Validade, cmbCategoria, cmbFornecedor, btnSalvar, btnCancelar , btnExcluir, btnVoltar);

	}

	private void initLayout() {
		lblTituloPagina.setLayoutX(400);
		lblTituloPagina.setLayoutY(10);

		lblTelaPrincipal.setLayoutX(450);
		lblTelaPrincipal.setLayoutY(320);

		lblTelaPrincipalImg.setLayoutX(450);
		lblTelaPrincipalImg.setLayoutY(250);
		
		
		lblCodigoProduto.setLayoutX(20);
		lblCodigoProduto.setLayoutY(80);
		txtCodigoProduto.setLayoutX(20);
		txtCodigoProduto.setLayoutY(100);
		
		lblDescricaoProduto.setLayoutX(250);
		lblDescricaoProduto.setLayoutY(80);
		txtDescricaoProduto.setLayoutX(250);
		txtDescricaoProduto.setLayoutY(100);
		txtDescricaoProduto.setPrefWidth(320);
		
		lblCategoria.setLayoutX(650);
		lblCategoria.setLayoutY(80);
		cmbCategoria.setLayoutX(650);
		cmbCategoria.setLayoutY(100);
		cmbCategoria.setPrefWidth(200);
		
		lblQuantidade.setLayoutX(20);
		lblQuantidade.setLayoutY(140);
		txtQuantidade.setLayoutX(20);
		txtQuantidade.setLayoutY(160);
		txtQuantidade.setPrefWidth(80);
		
		lblPrecoCompra.setLayoutX(170);
		lblPrecoCompra.setLayoutY(140);
		txtPrecoCompra.setLayoutX(170);
		txtPrecoCompra.setLayoutY(160);
		txtPrecoCompra.setPrefWidth(100);
		
		lblPrecoVenda.setLayoutX(330);
		lblPrecoVenda.setLayoutY(140);
		txtPrecoVenda.setLayoutX(330);
		txtPrecoVenda.setLayoutY(160);
		txtPrecoVenda.setPrefWidth(100);
		
		lblValidade.setLayoutX(490);
		lblValidade.setLayoutY(140);
		Validade.setLayoutX(490);
		Validade.setLayoutY(160);
		Validade.setPrefWidth(100);
		
		lblFornecedor.setLayoutX(650);
		lblFornecedor.setLayoutY(140);
		cmbFornecedor.setLayoutX(650);
		cmbFornecedor.setLayoutY(160);
		cmbFornecedor.setPrefWidth(300);
		
		btnSalvar.setLayoutX(200);
		btnSalvar.setLayoutY(550);
		btnSalvar.setPrefWidth(100);
		
		btnCancelar.setLayoutX(400);
		btnCancelar.setLayoutY(550);
		btnCancelar.setPrefWidth(100);
		
		btnExcluir.setLayoutX(600);
		btnExcluir.setLayoutY(550);
		btnExcluir.setPrefWidth(100);
		
		btnVoltar.setLayoutX(800);
		btnVoltar.setLayoutY(550);
		btnVoltar.setPrefWidth(100);

	}

	private void initListeners() {
		btnVoltar.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new TelaPrincipalView().start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) btnVoltar.getScene().getWindow();
				stage.close();

			}

		});

	}

	public static void main(String[] args) {
		launch(args);
	}
}
