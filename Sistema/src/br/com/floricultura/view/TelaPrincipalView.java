package br.com.floricultura.view;

import java.util.Optional;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import br.com.floricultura.connection.Conexao;
import br.com.floricultura.utility.MensagensAlerta;

public class TelaPrincipalView extends Application {

	private static Stage stage;
	private Pane painelPrincipal;
	private Label lblTituloPagina;
	private Label lblSairImg;
	
	private Label lblInicio;
	private Label lblVenda;
	private Label lblSair;
	private Label lblAjuda;
	private Label lblRelatorios;
	private Label lblProduto;
	private Label lblProdutoImg;
	private Label lblEstoque;
	private Label lblEstoqueImg;
	private Label lblRelatorio;
	private Label lblRelatorioImg;
	private Label lblCliente;
	private Label lblClienteImg;
	private Label lblFuncionario;
	private Label lblFuncionarioImg;
	
	Menu menuInicio;
	Menu menuVenda;
	Menu menuEstoque;
	Menu menuProduto;
	Menu menuFornecedor;
	Menu menuCliente;
	Menu menuFuncionario;
	Menu menuRelatorios;
	Menu menuAjuda;
	Menu menuSair;
	
	MenuItem menuItemProdutoForm;
	MenuItem menuItemProdutos;
	MenuItem menuItemFornecedorForm ;
	MenuItem menuItemFornecedores;
	MenuItem menuItemClienteForm;
	MenuItem menuItemClientes;
	MenuItem menuItemFuncionarioForm;
	MenuItem menuItemFuncionarios;


	private Image imgTeste;
	private Button btnSair;

	private String lblBorda = "-fx-border-color: #848484;" + "-fx-border-width: 3;";
	private String fonte = "-fx-font-size: 12pt;" + "-fx-font-family: \"Segoe UI Semibold\";\r\n"
			+ "-fx-text-fill: black;\r\n" + "-fx-opacity: 0.8;";

	private MensagensAlerta msgAlerta = new MensagensAlerta();

	@Override
	public void start(Stage stage) {
		try {

			initComponents();
			initListeners();
			initLayout();

			Scene scene = new Scene(painelPrincipal, 1024, 600);
			// stage.initStyle(StageStyle.UNDECORATED);
			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initLayout() {
		lblTituloPagina.setLayoutX(400);
		lblTituloPagina.setLayoutY(10);

		

		lblTituloPagina.setStyle("-fx-font-size: 25pt;" + "-fx-font-family: \"Segoe UI Semibold\";"
				+ "-fx-text-fill: black; " + "-fx-opacity: 0.8;");

		painelPrincipal.setStyle(lblBorda);

	}

	public static Stage getStage() {
		return stage;
	}

	private static void setStage(Stage stage) {
		TelaPrincipalView.stage = stage;

	}

	private void initComponents() {
		painelPrincipal = new Pane();
	

		lblTituloPagina = new Label("Tela Principal");
		lblInicio =  new Label("Inicio");
		lblVenda =  new Label("Venda");
		lblEstoque =  new Label("Estoque");
		lblRelatorios =  new Label("Ralatórios");
		lblAjuda =  new Label("Ajuda");
		lblSair =  new Label("Sair");
		
		menuInicio = new Menu("", lblInicio);
		menuVenda = new Menu("", lblVenda);
		menuEstoque = new Menu("", lblEstoque);
		menuProduto = new Menu("Produto");
		menuFornecedor = new Menu("Fornecedor");
		menuCliente= new Menu("Cliente");
		menuFuncionario = new Menu("Funcionario");
		menuRelatorios = new Menu("", lblRelatorios);
		menuAjuda = new Menu("",lblAjuda);
		menuSair = new Menu("", lblSair);
		
		menuItemProdutoForm = new MenuItem("Formulario Produtos");
		menuItemProdutos = new MenuItem("Produtos");
		menuItemFornecedorForm = new MenuItem("Formulario Fornecedores");
		menuItemFornecedores = new MenuItem("Fornecedores");
		menuItemClienteForm = new MenuItem("Formulário Clientes");
		menuItemClientes = new MenuItem("Clientes");
		menuItemFuncionarioForm = new MenuItem("Formulário Funcionários");
		menuItemFuncionarios = new MenuItem("Funcionários");
		
		MenuBar mb = new MenuBar();
		
		menuProduto.getItems().addAll(menuItemProdutoForm, menuItemProdutos);
		menuFornecedor.getItems().addAll(menuItemFornecedorForm, menuItemFornecedores );
		menuCliente.getItems().addAll(menuItemClienteForm, menuItemClientes);
		menuFuncionario.getItems().addAll(menuItemFuncionarioForm, menuItemFuncionarios);
		
		 mb.getMenus().addAll(menuInicio, menuVenda, menuEstoque, menuProduto, menuFornecedor, menuCliente, menuFuncionario, menuRelatorios, menuAjuda, menuSair);
		 mb.setLayoutX(3);
		 mb.setLayoutY(3);


		painelPrincipal.getChildren().addAll(mb);
	}

	private void initListeners() {
		Alert a = new Alert(AlertType.NONE);
		
		lblInicio.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new TelaPrincipalView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			

			}

		});
		
		lblVenda.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new VendaView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			

			}

		});
		
		lblEstoque.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new EstoqueView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			

			}

		});
		
		lblVenda.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new VendaView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			

			}

		});
		
		menuItemProdutos.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				
				try {
					new VendaView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
		});
		
		menuItemProdutos.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				
				try {
					new VendaView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
		});
		
		
		
		
		menuItemProdutoForm.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					
					try {
						new ProdutoView().start(new Stage());
						stage = (Stage) painelPrincipal.getScene().getWindow();
						stage.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
				}
			});
		
		menuItemProdutos.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					
					try {
						new ProdutoView().start(new Stage());
						stage = (Stage) painelPrincipal.getScene().getWindow();
						stage.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
				}
			});
		 
		menuItemFornecedorForm.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					
					try {
						new FornecedorView().start(new Stage());
						stage = (Stage) painelPrincipal.getScene().getWindow();
						stage.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
				}
			});
		
		menuItemFornecedores.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					
					try {
						new FornecedorTableView().start(new Stage());
						stage = (Stage) painelPrincipal.getScene().getWindow();
						stage.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
				}
			});
		
		menuItemClienteForm.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				
				try {
					new ClienteView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
		});
	
		menuItemClientes.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				
				try {
					new ClienteView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
		});
		
		
		menuItemFuncionarioForm.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				
				try {
					new FuncionarioView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
		});
	
		menuItemFuncionarios.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				
				try {
					new FuncionarioView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
		});
		
		
		lblRelatorios.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
		
					new RelatorioView().start(new Stage());
					stage = (Stage) painelPrincipal.getScene().getWindow();
					stage.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			

			}

		});
		
		lblAjuda.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
		
	                a.setAlertType(AlertType.INFORMATION);
	 
	                a.setContentText("Ajuda");
	                a.show();
				} catch (Exception e) {
					e.printStackTrace();
				}
			

			}

		});
	 
		 menuSair.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					
					try {
						new VendaView().start(new Stage());
						stage = (Stage) painelPrincipal.getScene().getWindow();
						stage.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
				}
			});
			

	}

	public static void main(String[] args) {
		//Conexao com = new Conexao();
		//com.conectarBanco();
		launch(args);
	}

}
