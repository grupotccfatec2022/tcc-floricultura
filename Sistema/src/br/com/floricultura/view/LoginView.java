package br.com.floricultura.view;

import br.com.floricultura.controller.LoginController;
import br.com.floricultura.utility.MensagensAlerta;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class LoginView extends Application {

	private static Stage stage;
	private Pane painelPrincipal;
	private TextField txtLogin;
	private PasswordField txtSenha;
	private Button btnEntrar, btnSair;
	private Label lblLogin;
	private ImageView imgView;
	
	private MensagensAlerta msgAlerta = new MensagensAlerta();
	@Override
	public void start(Stage stage) throws Exception {
		initComponents();
		initListeners();
		Scene scene = new Scene(painelPrincipal);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setTitle("Login");
		stage.show();

		initLayout();
		LoginView.setStage(stage);
	}

	public static void setStage(Stage stage) {
		LoginView.stage = stage;
	}

	public static Stage getStage() {
		return stage;
	}

	private void initComponents() {
		painelPrincipal = new Pane();
		painelPrincipal.setPrefSize(450, 450);
		painelPrincipal.setStyle("-fx-background-color:linear-gradient(" + "from 0% 0% to 100% 100%, #585858 40%, #000000 100%);");
		
		
		lblLogin = new Label("Login");
		lblLogin.setStyle("-fx-text-fill:WHITE; -fx-font-size: 40;");
		txtLogin = new TextField();
		txtLogin.setPromptText("Digite o Login...");

		txtSenha = new PasswordField();
		txtSenha.setPromptText("Senha");

		btnEntrar = new Button("Entrar");
		btnSair = new Button("Sair");

		painelPrincipal.getChildren().addAll(lblLogin, txtLogin, txtSenha, btnEntrar, btnSair);
	}

	private void initLayout() {

		lblLogin.setLayoutX(175);
		lblLogin.setLayoutY(50);
	
	
		txtLogin.setLayoutX(100);
		txtLogin.setLayoutY(150);
		txtLogin.setPrefSize(250, 30);

		txtSenha.setLayoutX(100);
		txtSenha.setLayoutY(200);
		txtSenha.setPrefSize(250, 30);
		
		btnEntrar.setLayoutX(100);
		btnEntrar.setLayoutY(250);
		btnEntrar.setPrefSize(100, 30);
		
		
		btnSair.setLayoutX(250);
		btnSair.setLayoutY(250);
		btnSair.setPrefSize(100, 30);
		
	}

	private void initListeners() {

		btnEntrar.setOnAction(new EventHandler<ActionEvent>() {
			LoginController log = new LoginController();
			@Override
			public void handle(ActionEvent event) {
				
				try {
					if(txtLogin.getText().trim().isEmpty() || txtLogin.getText() == null ) {
						msgAlerta.mensagemInformativa("Campo Login", null, "Campo Login Vazio");
						
					}else if(txtSenha.getText().trim().isEmpty() || txtSenha.getText() == null) {
				
						msgAlerta.mensagemInformativa("Campo Senha", null, "Campo Senha Vazio");
					}else {
						if(log.logar(txtLogin.getText(), txtSenha.getText())) {
							new TelaPrincipalView().start(new Stage());
							msgAlerta.mensagemInformativa("Logar", null, "Logado Com Sucesso");
							LoginView.stage.close();
							Stage stage = (Stage) btnEntrar.getScene().getWindow();
							stage.close();
						}else {

							msgAlerta.mensagemInformativa("Logar", null, "Usuario ou Senha Invalida");
						}
					}
					
				} catch (Exception e) {
					
					e.printStackTrace();
				}

			}
		});

		btnSair.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				LoginView.stage.close();

			}
		});
	}
	
	

	public static void main(String[] args) {
		launch(args);
	}

}
