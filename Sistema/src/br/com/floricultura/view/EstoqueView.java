package br.com.floricultura.view;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class EstoqueView extends Application {

	private static Stage stage;
	private Pane painelPrincipal;
	private Label lblTituloPagina;
	private Label lblTelaPrincipal;
	private Label lblTelaPrincipalImg;
	private Image imgTelaPrincipal;
	
	@Override
	public void start(Stage stage) throws Exception {
		try {

			initComponents();
			initListeners();
			initLayout();

			Scene scene = new Scene(painelPrincipal, 1024, 600);

			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Stage getStage() {
		return stage;
	}

	private static void setStage(Stage stage) {
		EstoqueView.stage = stage;

	}

	private void initLayout() {
		lblTituloPagina.setLayoutX(400);
		lblTituloPagina.setLayoutY(10);
		
		lblTelaPrincipal.setLayoutX(450);
		lblTelaPrincipal.setLayoutY(320);
		
		lblTelaPrincipalImg.setLayoutX(450);
		lblTelaPrincipalImg.setLayoutY(250);
		

	}

	private void initListeners() {
		lblTelaPrincipalImg.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					new TelaPrincipalView().start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) lblTelaPrincipalImg.getScene().getWindow();
				stage.close();
				
			}
			
		}); 

	}

	private void initComponents() {
		painelPrincipal = new Pane();
		lblTelaPrincipalImg = new Label();
		lblTelaPrincipal = new Label("Tela EstoqueView");
		lblTituloPagina = new Label("Menu Principal");
		
		imgTelaPrincipal = new Image(getClass().getResourceAsStream("/imagens/testeImg.png"));
		lblTelaPrincipalImg.setGraphic(new ImageView(imgTelaPrincipal));
		
		painelPrincipal.getChildren().addAll(lblTituloPagina, lblTelaPrincipalImg, lblTelaPrincipal);

	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
