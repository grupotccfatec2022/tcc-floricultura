package br.com.floricultura.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.floricultura.connection.Conexao;

public class LoginDAO {
	private Connection conexao = null;
	private PreparedStatement statement = null;
	private ResultSet resultSet = null;
	
	
	public boolean logar(String login, String senha) {
		Conexao con = new Conexao();
		conexao = con.conectarBanco();
		
		try {

			String sql = "SELECT * FROM USUARIO WHERE LOGINUSUARIO = ? AND SENHAUSUARIO = ?";

			statement = conexao.prepareStatement(sql);
			statement.setString(1, login );
			statement.setString(2, senha );
			
			resultSet = statement.executeQuery();

			return resultSet.next() ;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			con.fechaConexao();
			
		}
		return false;
		
	}
}
