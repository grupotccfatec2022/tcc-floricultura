package br.com.floricultura.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import br.com.floricultura.connection.Conexao;
import br.com.floricultura.model.CaixaModel;

public class CaixaDAO {
	
	private Connection conexao = null;
	Conexao con = null;
	private PreparedStatement statement = null;
	private ResultSet resultSet = null;
	
	public void abrirCaixa(CaixaModel caixa) {
		con = new Conexao();
		conexao = con.conectarBanco();
		
		try {

			String sql = "INSERT INTO CAIXA( STATUSCAIXA, VALORINICIALCAIXA, DATACAIXA)"
					+ "VALUES(?, ?, GETDATE())";

			statement = conexao.prepareStatement(sql);
			
			statement.setString(1, caixa.getStatusCaixa());
			statement.setBigDecimal(2, caixa.getValorInicial());
			
			statement.executeUpdate();

			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			con.fechaConexao();
			
		}
		
	}
	
	public void fecharCaixa(CaixaModel caixa) {
		con = new Conexao();
		conexao = con.conectarBanco();
		
		try {

			String sql = "UPDATE SET STATUSCAIXA = ?, VALORFINALCAIXA = ?";
				
			statement = conexao.prepareStatement(sql);
			
			statement.setString(1, caixa.getStatusCaixa());
			statement.setBigDecimal(2, caixa.getValorFinal());
			
			statement.executeUpdate();

			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			con.fechaConexao();
			
		}
		
	}
	
	public CaixaModel buscarCaixaPorData(LocalDate data) {
		con = new Conexao();
		conexao = con.conectarBanco();
		CaixaModel caixa = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String dataNova = formatter.format(data);
		
		
		try {
			

			String sql = "SELECT CODCAIXA, STATUSCAIXA, VALORFINALCAIXA, VALORINICIALCAIXA, DATACAIXA"
					+ "WHERE DATACAIXA = '" + dataNova + "'" ;
				
			statement = conexao.prepareStatement(sql);
			resultSet = statement.executeQuery();
			
			System.out.println(sql);
			
			while(resultSet.next()) {
				caixa = new CaixaModel();
				
				caixa.setCodCaixa(resultSet.getInt("CODCAIXA"));
				caixa.setStatusCaixa(resultSet.getString("STATUSCAIXA"));
				caixa.setValorFinal(resultSet.getBigDecimal("VALORFINALCAIXA"));
				caixa.setValorInicial(resultSet.getBigDecimal("VALORINICIALCAIXA"));
				caixa.setDataEntradaCaixa(LocalDate.parse(resultSet.getString("DATACAIXA"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
				
			}
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			con.fechaConexao();
			
		}
		
		return caixa;
		
	}
}
