package br.com.floricultura.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.floricultura.connection.Conexao;
import br.com.floricultura.model.FornecedorModel;

public class FornecedorDAO {
	
	private Connection conexao = null;
	Conexao con = null;
	private PreparedStatement statement = null;
	private ResultSet resultSet = null;

	
	public boolean incluirFornecedor(FornecedorModel fornecedor) {
		con = new Conexao();
		conexao = con.conectarBanco();
		boolean retorno = false;
		
		try {

			String sql = "INSERT INTO FORNECEDOR( CODFORNECEDOR, NOMEFORNECEDOR, CNPJFORNECEDOR ,STATUSFORNECEDOR)"
					+ "VALUES(?, ?, ?, ?)";

			statement = conexao.prepareStatement(sql);
			
			statement.setInt(1, 4);
			statement.setString(2, fornecedor.getRazaoSocial());
			statement.setString(3, fornecedor.getCpfCnpj());
			statement.setString(4, "Ativo");
			
			statement.executeUpdate();
			
			retorno = true;
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			con.fechaConexao();
			
		}
		System.out.println(fornecedor);
		return retorno;
	}

	public boolean alterarFornecedor(FornecedorModel fornecedor) {
		con = new Conexao();
		conexao = con.conectarBanco();
		boolean retorno = false;
		
		try {

			String sql = "UPDATE FORNECEDOR SET NOMEFORNECEDOR = ?, CNPJFORNECEDOR = ? WHERE CODFORNECEDOR = ? AND STATUSFORNECEDOR = 'ATIVO'";
	
			statement = conexao.prepareStatement(sql);
			statement.setString(1, fornecedor.getRazaoSocial());
			statement.setString(2, fornecedor.getCpfCnpj());
			statement.setInt(3, fornecedor.getCodFornecedor());
		
			statement.executeUpdate();
			
			retorno = true;
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			con.fechaConexao();
			
		}
	
		return retorno;
	}

	public boolean excluirFornecedor(int idFornecedor) {
		con = new Conexao();
		conexao = con.conectarBanco();
		boolean retorno = false;
		
		try {

			String sql = "UPDATE FORNECEDOR SET STATUSFORNECEDOR = ? WHERE CODFORNECEDOR = ?";
	
			statement = conexao.prepareStatement(sql);
			statement.setString(1, "INATIVO");
			statement.setInt(2, idFornecedor);
		
			statement.executeUpdate();
			
			retorno = true;
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			con.fechaConexao();
			
		}
	
		return retorno;
	}

	public FornecedorModel buscarFornecedorPorId(FornecedorModel fornecedor) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<FornecedorModel> listarFornecedores() {
		con = new Conexao();
		conexao = con.conectarBanco();
		
		List<FornecedorModel> listaFornecedor = new ArrayList();
		try {

			String sql = "SELECT * FROM FORNECEDOR WHERE STATUSFORNECEDOR <> 'INATIVO' ";
			statement = conexao.prepareStatement(sql);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				FornecedorModel fornecedor = new FornecedorModel();
				fornecedor.setCodFornecedor(resultSet.getInt("CODFORNECEDOR"));
				fornecedor.setRazaoSocial(resultSet.getString("NOMEFORNECEDOR"));
				fornecedor.setCpfCnpj(resultSet.getString("CNPJFORNECEDOR"));
			
				listaFornecedor.add(fornecedor);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.fechaConexao();
			try {
				statement.close();
				resultSet.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return listaFornecedor;
	}

	

}
